// grab the mongoose module
var mongoose         = require('mongoose');

var BookSchema = new mongoose.Schema({ any: mongoose.Schema.Types.Mixed }, { strict: false });


// define our nerd model
// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Book', BookSchema);
