var basicAuth   = require('basic-auth');
var formidable  = require('formidable');
var path        = require('path');
var fs          = require('fs');
var uuid        = require('node-uuid');
var mongoose    = require('mongoose');


// MODELS
var BookModel   = require('./models/Book.js');

// APP PATHS
var appRootPath = path.resolve(__dirname, '../');
var uploadPath  = path.join(appRootPath, 'public/upload/');
var imgRelPath  = 'upload/';


// UTILES
var fileUpload = function(req, callback){

    var form = new formidable.IncomingForm();
    // form.uploadDir = uploadPath;
    var fileName;
    var format;

    form.on('file', function(field, file) {
        // get file ext. name e.g '.ppt', '.pub' ...
        format = path.extname(file.name);
        
        //remove dot from the ext. e.g -> ".html" becomes 'html'
        // cleanFormat = format.substring(1);

        // rename the incoming file to the file's name
        // fileName = 'bookfile' + format;

        fileName = saveFile(file.path, format);
    })
    .on('error', function(err) {
        console.log("an error has occured with form upload");
        console.log(err);
        req.resume();
    });
        
    form.parse(req, function(){
        callback(fileName, format);
    });
}

var createUploadFolderIfNotExist = function(dir){
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
};

var mkdirSync = function (path) {
  try {
    fs.mkdirSync(path);
  } catch(e) {
    if ( e.code != 'EEXIST' ) throw e;
  }
};

var saveFile = function(filePath, format){
    // Generate book folder name
    // var bookFolderName = uuid.v4();

    var fileName = uuid.v4() + format;
        
    // create book folder
    // mkdirSync(path.join(uploadPath, bookFolderName));

    // get final path of book folder
    // var bookFolderPath = path.join(uploadPath, bookFolderName);
    
    // save file
    fs.rename(filePath, path.join(uploadPath, fileName));

    console.log("The book ("+ path.join(uploadPath, fileName) +") has been uploaded");

    return fileName;
};

// MIDDLEWARE
var createUploadFolder = function(req, res, next){
    // create upload folder
    createUploadFolderIfNotExist(uploadPath);

    return next();
}

var auth = function(req, res, next) {
    function unauthorized(res) {
        // Add a comment to this line
        res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
        return res.send(401);
    };

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
        return unauthorized(res);
    };

    if (user.name === 'user' && user.pass === 'user!') {
        return next();
    } else {
        return unauthorized(res);
    };
};

module.exports = function(app) {

	// server routes ===========================================================
	// handle things like api calls

    // BOOKS

    app.post('/api/createBook', function(req, res){
        var bookData = req.body.bookData;

        var scrapeId = req.body.scrapeId ? mongoose.Types.ObjectId(req.body.scrapeId) : false;

        var book = {data:bookData};

        book.linkedScrapes = scrapeId ? [scrapeId] : [];

        var bookEntity = new BookModel(book);
        bookEntity.save(function (err, data) {
            if (err) {
                res.send({error: err});
                return;
            }
          
            console.log('Book has been saved');
            res.send(201);

        });
    });


    app.get('/api/getBooks', function(req, res){
        BookModel.find(function(err, data) {
            if (err) return console.error(err);
            res.send(data);
        })
    });


    app.post('/api/deleteBook', function(req, res){
        var bookId = mongoose.Types.ObjectId(req.body.id)

        BookModel.remove({
            _id: bookId
        }, function(err) {
            if (err) {
                res.status(400).send(err);
                return;
            }

            console.log('Book has been removed and Scrapes that have been assosiated have been cleared');
            res.send(201);
        });

    });



    // frontend routes =========================================================
    // route to handle all angular requests
    app.get('*', auth, function(req, res) {
        res.render('index', { title: 'Hey', message: 'Hello there!'});
    });
};