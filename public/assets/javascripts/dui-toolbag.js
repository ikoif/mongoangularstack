angular.module("dui.toolbag", ["dui.toolbag.modal"]);
angular.module("dui.toolbag.modal", [])
    .directive('modal', [function () {

/*

Author: Ivaylo Danev

Use:

add the directive element where you are going to use it and set the data value
of 'modal' to the scope object of the modal state

in example:
    in the html add:
        modal(name='myModal', width='750px', height='60%')
            p Modal Content Goes here

    in the controller:
        // this will set the modal to be hidden initially
        $scope.myModal = false;

then to trigger the modal to show or hide you simply change the value of the
variable that was set for this modal, in example:
    <button ng-click="myModal = !myModal">Toggle My Modle</button>

this will set the value of myModal to the reverse of its current value
and will trigger the show/hide procedures of the modal directive

this directive uses an isolated scope in order to keep the instance of
the modal isolated from the parent scope and uses an object defined
in the parent scope to comunicate with the directive through watch and
the directive to trigger functions for closing/hiding and preserving the
double way binding of the scope variable which sets the modal show state
to true or false

attributes:

name="[String]"
    the name of the variable defined in the scope that will be used for communication
    between the controller scope and the isolated directive scope to trigger
    show/hide functionality

width="[String]"
    defines the width of the modal in pixels, if empty or not defined will
    take the .container width (which is resposive and the width of the screen)

height="[String]"
    defines the height of the modal in pixels, if empty or not defined will
    take the make the content as high as possible based on content and will
    add padding if edges of the screen are reached

padding="[String]"
    defines the padding that will be added on the top and bottom of the modal
    before the edge of the screen is reached when height not enough or content
    is too much

modalClass="[String]"
    adds this class to the .modal-hld container, this class can be added in
    _modal.scss. If not defined .modal-default will be used.

closeButton="[Boolean]"
    will cause the close button to either be hidden or displayed
    true will show it, false will hide it

disable-closing="[Boolean]"
    disables the closing of the modal when clicking on the overlay div
    default value: false
    set true to enable it

manipulatedom="[String] - name of model"
    set the value to be equals to the name of model from parrent scope
    With this now you can manipulate models from the parent scope
    directly in this modal scope


modalClass="transparent OR framed OR [String]"
    - transparent
        if transparent is used it will make the modal frameless so anything can be
        put in the modal content and will be displayed in the center, perfect for loaders
    - framed
        if framed is used, it will use the design of a frame and put the contents
        inside, when this is used it needs two additional properties, "icon" and "title"
    - [String]
        any additional class can be used here which will add that class to .modal-hld
        it will me added with a "modal-" prefix, so if custom modal design needs to be
        created, you can define modal-'String' in the css and style that custom design.
        this can be added in styles/partials/_modal.scss

title=[String]
    adds a title, used in conjunction with modal-class="framed"

icon=[String]
    defines the icon of the modal, if none defined, a default one will be used,
    used in conjunction with modal-class="framed"

funcs=[Object / Angular scope object / JSON object]
    if any additional functionality needs to be used in the scope of the directive,
    (for example sharing modal, the sharing functionality needs to be passed to the
    modal as it cant be accessed from the scope of the controller due to the isolated
    scope) pass a JSON object to this attribute so it can be used inside the modal, see
    bookController.js  $scope.share object for example of use.






=========================================================================================
=============================   CREATING STEPS IN A MODAL   =============================
=========================================================================================

To create steps in a modal, follow these steps

add class "step" to all the divs that will be steps, and the first one to have
class "active" as well, for example:

    modal(name="exampleModal", modal-class="framed", title="My process")
        .step.active
            div this is step 1
        .step
            div this is step 2

each step has its own title, icon and optional width, they are added
as data tags to the div itself:

    modal(name="exampleModal", modal-class="framed", title="My process")
        .step.active(data-title="My first step!", data-icon="eye", width="200px")
            div this is step 1
        .step(data-title="My second step!", data-icon="eye", width="400px")
            div this is step 2

NOTE: the initlal title here will be "My process", but if returned to that step1
            the title will become the title that is passed to step1. Same goes for the
            width, so a good rule of thumbs is to always have the same title, icon and 
            width that is defined in the modal, to keep everything concistent (or not, 
            your choice). Also if width is passed to the modal, that will be the 
            general width used in the modal, any data-width that is passed to the steps
            will only be applied for that step and the rest will obey the width of the 
            modal.

To control the steps, use these two functions:

    To go forward:
        ng-click="stepEvents.next($event)"

    To go backward:
        ng-click="stepEvents.prev($event)"

    To go to a specific step:
        ng-click="stepEvents.step($event, 'jQuery selector to the step that has 
        .step-manual class')"

So continuing our example, this will be added like so:

    modal(name="exampleModal", modal-class="framed", title="myProcess", icon="fork")
        .step.active
            div this is step 1
            div(ng-click="stepEvents.next($event") Next step
        .step
            div this is step 2
            div(ng-click="stepEvents.prev($event)") Previous step
            div(ng-click="stepEvents.step($event, '#login')") Go to manual step
        .step-manual#login
            div this is a manual step, for example you only need to go to if not logged in
            div(ng-click="stepEvents.prev($event)") Previous step

NOTE: manual steps are skipped when moving with next and prev, but when same is
            called from a manual step, it will move to the first .step. Their purpouse
            is conditional steps


when the hideModal() function is called, it will reset all of the steps, so the user will
be returned to the first div with the class ".step"


*/

        return {
            restrict: 'EA',
            scope: {
                modal: '=name',
                funcs: '=',
                manipulatedom: '='
            },
            replace: true, // Replace with the template below
            transclude: true, // we want to insert custom content inside the directive
            link: function(scope, element, attrs, ctrl, transclude) {

                // scope.oninit();
                // console.log(scope.oninit)

                //- false by default, requires a value first before it can be used
                var attrWidth = false;

                //- false by default to hide the close button element if it's not defined as an attribute
                scope.hideCloseButton = false;

                //- false by default, if it is set to true, the user cannot close the modal by clicking on the overlay div
                scope.disableClosing = false;

                //- false by default to hide the title element if title is not defined as an attribute
                scope.title = false;

                //- false by default to hide the icon element if icon is not defined as an attribute
                scope.icon = false;

                //- defines if the modal will be framed or frameless
                scope.framed = false;

                // scope.$apply(scope.hideCloseButton);

                // variable for quick check if modal is displayed for mobile
                var isMobile = false;

                if(attrs.width){
                    attrWidth = attrs.width;
                }

                if (attrs.modalClass) {
                    if (attrs.modalClass === 'framed') {

                        //- defines if the modal as a framed one
                        scope.framed = true;

                        $(element).find('.modal').addClass('modal-framed');

                        if (attrs.title) {
                           scope.title = attrs.title;
                        }

                        if (attrs.icon) {
                            scope.icon = attrs.icon;
                        }else{
                            scope.icon = 'book';
                        }

                    } else {
                        $(element).find('.modal').addClass('modal-' + attrs.modalClass);
                    }
                } else {
                    $(element).find('.modal').addClass('modal-default');
                }

                if (attrs.closeButton) {
                    scope.hideCloseButton = true;
                }
                //this attribute is used to prevent user from closing the modal
                //by clicking on the overlay div
                if (attrs.disableClosing) {
                    scope.disableClosing = true;
                }

                // procedures which are done when showing the modal
                scope.showModal = function () {


                    if($(window).width() <= 768){
                        isMobile = true;

                        if(attrs.closeButton === 'false'){
                            scope.hideCloseButton = false;
                        }else{
                            scope.hideCloseButton = true;
                        }
                        
                        $('#content').addClass('modal-open');

                        var screenHeight = $(window).height() - $('#header-hld').height();
                        $('#content').css('height', screenHeight);

                    }else{
                        isMobile = false;
                    }

                    if($(window).width() >= 768 || attrs.mobilePage === undefined){
                        $(element).show();
                        // $('body').css('overflow','hidden');

                        $('body').addClass('modal-open');
                        // call the resizer once before starting the listener
                        // so it can get its dimention values before being shown
                        // for the first time
                        scope.modalEvents.startResize();

                    }else{
                        // TO DO ! TO DO TO DO
                        window.location.href = attrs.mobilePage;
                    }
                    
                    
                };

                // procedures which are done when closing/hiding the modal
                scope.hideModal = function() {
                    scope.modalEvents.endResize();
                    $(element).hide();
                    scope.stepEvents.reset();
                    $('#content').removeClass('modal-open');
                    $('body').removeClass('modal-open');
                    $('#content').css('height','auto');
                };

                // this is to be used from the scope of the directive - the close button
                // the hideModal func is used
                scope.closeModal = function() {
                    scope.modal = false;
                };

                scope.modalEvents = {
                    startResize: function () {
                        // $(window).resize(scope.modalEvents.modalResizer);
                        scope.modalEvents.modalResizer();
                        $(document).keyup(scope.modalEvents.escClose);
                    },
                    endResize: function () {
                        // $(window).off('resize', scope.modalEvents.modalResizer);
                        $(document).unbind('keyup', scope.modalEvents.escClose);
                    },
                    modalResizer: function () {
                        var topPadding;
                        var modalWidth;
                        var iconHeight = 0;

                        var padding = {
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0
                        };

                        var paddingTitle = {
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0
                        };

                        var $modalHld = $(element).find('.modal-hld');
                        var $content = $modalHld.children('.content');
                        var $contentTitle = $modalHld.children('.title-hld');
/*
                        padding.top = parseInt($content.css('padding-top').replace('px', ''));
                        padding.bottom = parseInt($content.css('padding-bottom').replace('px', ''));
                        padding.left = parseInt($content.css('padding-left').replace('px', ''));
                        padding.right = parseInt($content.css('padding-right').replace('px', ''));

                        if(scope.framed){

                            paddingTitle.top = parseInt($contentTitle.css('padding-top').replace('px', ''));
                            paddingTitle.bottom = parseInt($contentTitle.css('padding-bottom').replace('px', ''));
                            paddingTitle.left = parseInt($contentTitle.css('padding-left').replace('px', ''));
                            paddingTitle.right = parseInt($contentTitle.css('padding-right').replace('px', ''));
                        }

                        if(attrs.padding){
                            topPadding = attrs.padding;
                        }else{
                            topPadding = 40;
                        }

                        iconHeight = $modalHld.children('.title-hld').children('.icon-hld').css('height');

                        if(iconHeight){
                            iconHeight = parseInt(iconHeight.substr(0, iconHeight.length-2));
                            console.log(iconHeight)
                        }*/

                        if(attrWidth){
                            modalWidth = attrWidth;
                        }else{
                            modalWidth = $('.container').width();
                        }

                        if(typeof(modalWidth) === 'string'){
                            modalWidth = parseInt(modalWidth.substr(0, modalWidth.length-2));
                        }

                        if(isMobile){
                            modalWidth = 'auto'
                        }

                        $modalHld.css('width', modalWidth);
                        // console.log(modalWidth, 'wiiiiiidddhtththththt')

                        // if the modal is shown on mobile, do not adjust margins as the
                        // position of the modal will be static
                        
                        // if(!isMobile){
                        //     $modalHld.css('margin-left', -(modalWidth/2))
                        // }

                        // //- find out what is the largest possible value for the height
                        // var modalHeightDef = ($(window).height() - ((topPadding*2) + (iconHeight/4)));

                        // $content.css('height', 'auto');

                        // var modalHeight = $content.height() + padding.top + padding.bottom;

                        // var titleHeight = 0;

                        // if(scope.framed){
                        //     //- if the modal is framed, add the title to the height
                        //     titleHeight = $contentTitle.height() + paddingTitle.top + paddingTitle.bottom;
                        //     modalHeight += titleHeight;
                        // }

                        // console.log(modalHeight + ">" + modalHeightDef)
                        // if(modalHeight > modalHeightDef){

                        //     //- modal's height is too large to fit on the screen
                        //     modalHeight = modalHeightDef;

                        //     //- assign the height of the content if the modal is framed
                        //     if(scope.framed){
                        //         $content.css('height', modalHeight - titleHeight)
                        //     }else{
                        //         $content.css('height', modalHeight);
                        //     }
                        // }

                        // if(isMobile){
                        //     $modalHld.css('height', 'auto');
                        //     $modalHld.css('margin', 'auto');
                        //     $modalHld.css('margin-top', iconHeight);
                        // }else{
                        //     $modalHld.css('height', modalHeight);
                        //     $modalHld.css('margin-top', -((modalHeight/2)-iconHeight/2));
                        // }
                        


                        // $(element).children('.modal-hld').css('margin-top',-(modalHeight)/2)

                    },
                    escClose: function (e) {
                        if (e.keyCode === 27 && !scope.disableClosing) { scope.modal = false; scope.$apply(scope.modal); }
                    },
                    applyAttributes: function (nextElement){
                        //nextElement is a cached jquery object

                        if(nextElement){

                            if(nextElement.data('title')){
                                scope.title = nextElement.data('title');
                            }
                            if(nextElement.data('icon')){
                                scope.icon = nextElement.data('icon');
                            }
                            if(nextElement.data('width')){
                                if(nextElement.data('width') !== 'auto'){
                                    attrWidth = nextElement.data('width');
                                    //- alert(attrWidth + 'has width')
                                    scope.modalEvents.modalResizer();     
                                }else{
                                    attrWidth = false;
                                    scope.modalEvents.modalResizer();
                                }
                            }else{
                                if(attrs.width){
                                    attrWidth = attrs.width;
                                    //- alert(attrWidth + 'uses attrs.width')
                                }else{
                                    //- alert('doesnt have anything')
                                }
                                scope.modalEvents.modalResizer();
                            }

                        }//- end of if

                    }//- end of applyAttributes
                };

                scope.stepEvents = {
                    next: function ($event) {
                        var $this = $(angular.element($event.currentTarget));
                        var $parent = $this.closest('.step');
                        var $nextElement = false;
                        if(!$parent.length){
                            $parent = $this.closest('.step-manual');
                        }
                        if($parent.hasClass('active')){
                            $parent.removeClass('active');
                            $nextElement = $parent.nextAll('.step:first');
                            $nextElement.addClass('active');
                            scope.modalEvents.modalResizer();
                        }

                        scope.modalEvents.applyAttributes($nextElement);
                    },
                    prev: function ($event) {
                        var $this = $(angular.element($event.currentTarget));
                        var $parent = $this.closest('.step');
                        var $nextElement = false;
                        if(!$parent.length){
                            $parent = $this.closest('.step-manual');
                        }
                        if($parent.hasClass('active')){
                            $parent.removeClass('active');
                            $nextElement = $parent.prevAll('.step:first');
                            $nextElement.addClass('active');
                            scope.modalEvents.modalResizer();
                        }

                        scope.modalEvents.applyAttributes($nextElement);
                    },
                    step: function ($event, name) {
                        var $nextElement = false;
                        if($(name)) {
                            var $content = $(element).find('.content');
                            $content.children('.active').removeClass('active');
                            $nextElement = $content.children(name);
                            $nextElement.addClass('active');

                            scope.modalEvents.modalResizer();

                            scope.modalEvents.applyAttributes($nextElement);
                        }
                    },
                    reset: function () {
                        // var $content = $(element).find('.content');
                        // $content.children('.active').removeClass('active');
                        // $content.children('.step').addClass('active');
                    }
                };

                // watch for the object that was passed through 'modal'
                scope.$watch('modal', function(newVal, oldVal){

                    // To detect this scenario within the listener fn,
                    // you can compare the newVal and oldVal. If these two values
                    // are identical (===) then the listener was called due
                    // to initialization.
                    // ref(https://docs.angularjs.org/api/ng/type/$rootScope.Scope)

                    if(newVal !== oldVal){

                        // check if value of the 'modal' directive was set to true or false
                        // in order to trigger the 'show' or 'hide' procedures
                        if(newVal){
                            scope.showModal();
                        }else{
                            scope.hideModal();
                        }
                    }
                }, true);

                transclude(scope, function(nodes) {
                    angular.element(element[0].querySelector('.content')).append(nodes);
                });

                //- show the modal if 'true' by default
                if(scope.modal){
                    scope.showModal();
                }
            },
            templateUrl: '/views/partials/modal/modal.html'
        };
    }]);