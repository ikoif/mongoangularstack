'use strict';

/**
 * Top level module. Lists all the other modules as dependencies.
 */

angular
    .module('bookinfo', [
        'ngRoute',
        'angular-loading-bar',
        'ngMaterial',
        'bookinfo.all',
        'ngTable',
        'ngTextTruncate'
    ])
    .value('$const', {
        IMAGE: {
            user: {
                ph: '/images/avatar.png'
            },
            loader: '/images/loader.gif'
        }
    })

    .config(function ($routeProvider, $locationProvider, cfpLoadingBarProvider, $mdThemingProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('amber', {
                'default': '500',
                'hue-1': '300',
                'hue-2': '600',
                'hue-3': 'A100'
            })
            .accentPalette('blue-grey', {
                'default': '500',
                'hue-1': '300',
                'hue-2': '600',
                'hue-3': 'A100'
            });
        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.latencyThreshold = 500;//ms
        $locationProvider.html5Mode(true);
        $routeProvider
            .otherwise({
                redirectTo: '/'
            });
    })

    .run(function ($location, $rootScope, $window, $route, api) {

        $rootScope.$on('$routeChangeStart', function (ev, to, toParams, from, fromParams) {
            // run phase reached so show body, against FOUC

            if($('#body').css('display') !== 'block'){
                $('#body').show();
                $('#loading').hide();
            }
        });

        // set actions to be taken each time the user navigates
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            // set page title
            $rootScope.currentLocation = $location.path();
            window.scrollTo(0,0)

        });

        $rootScope.$on("$routeChangeError", function(evt, current, previous, rejection) {
            // if (rejection == "not_logged_in") {
            //     //DO SOMETHING
            // }
        });

        // attach commonly used info to root scope to be available to all controllers/views
        $rootScope.common = $rootScope.common || {
            active: {},
            logout: function () {
                delete $window.sessionStorage.token;
                delete $window.sessionStorage.user;
                delete $window.localStorage.token;
                delete $window.localStorage.user;
                $window.location.replace('/logout');
            },
            clearDatabase: function () {
                var self = this;
            }
        };
    })
    .controller('layoutController', ['$scope', '$rootScope', 'api', function($scope, $rootScope, api){
        $scope.$rootScope = $rootScope;

    }]);