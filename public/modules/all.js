'use strict';

/**
 * Module definition for common components used by other all other app modules.
 */

angular.module('bookinfo.all', [
	'bookinfo.home',
	'bookinfo.common',
]);