'use strict';

/**
 * Home module for displaying home page content.
 */

angular
    .module('bookinfo.home', [
      'ngRoute',
      'monospaced.elastic',
      'bookinfo.common'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                title: 'Bookinfo - Информационен поток',
                templateUrl: 'modules/home/home.html',
                controller: 'HomeCtrl',
                resolve: {
                    initialData: ['initialDataHome', function (initialDataHome) {
                        return initialDataHome();
                    }]
                }
            });
    })
    .factory("initialDataHome",
    ['notification', '$route', '$q', '$timeout', 'api', '$location', 'Content.user', 'convert', 'dataDiff', '$const',
    function (notification, $route, $q, $timeout, api, $location, cUser, convert, dataDiff, $const) {
        return function () {
            var loadUserData = function () {
                var deferred = $q.defer();

                // cUser.getUser().then(function(user){
                //     deferred.resolve(user);
                // });

                deferred.resolve('user');

                return deferred.promise;
            };

            return $q.all([
                loadUserData()
            ]).then(function (results) {
                return {
                    user: results[0]
                };
            })
            .catch(function (err) {
                // console.log('Resolve error ', err);
            });
        };
    }]);