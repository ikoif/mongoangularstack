'use strict';

/**
 * Home controller simply lists all the posts from everyone on the front page.
 */

angular.module('bookinfo.home').controller('HomeCtrl', function ($scope, $route, $mdDialog, $mdMedia, $rootScope, api, convert, $const, notification, $http) {
	$scope.$rootScope = $rootScope;

    $scope.books = [];

    $scope.remove = function (id){
        $http({
            method: 'POST',
            url: 'api/deleteBook',
            data: {"id":id}
        }).then(function successCallback(res) {
            // $scope.books = res;
            $scope.loadBooks();
            notification('success', 'Deleted successfully')
        }, function errorCallback(err) {
            notification('error', err.data)
        });
    }

    $scope.loadBooks = function() {
        $http({
            method: 'GET',
            url: 'api/getBooks'
        }).then(function successCallback(res) {
            console.log(res);
            $scope.books = res.data;
        }, function errorCallback(err) {
            notification('error', err)
        });
    };

    $scope.loadBooks();


    $scope.showRemoveConfirm = function(ev, id) {
        // Appending dialog to document.body to cover sidenav in docs app
        var confirm = $mdDialog.confirm()
            .title('Delete this book?')
            .targetEvent(ev)
            .ok('Yes')
            .cancel('Cancel');
        $mdDialog.show(confirm).then(function() {
            $scope.remove(id);
        }, function() {
        });
    };


    $scope.showAdvanced = function(ev) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: DialogController,
            templateUrl: '/modules/home/create-book.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen
        })
        .then(function(data) {
            if(data){
                $http({
                    method: 'POST',
                    url: 'api/createBook',
                    data: {"bookData" : data}
                }).then(function successCallback(res) {
                    // $scope.books = res;
                    $scope.loadBooks();
                    notification('success', res.data)
                }, function errorCallback(err) {
                    notification('error', err.data)
                });
            }
        }, function() {
            $scope.status = 'You cancelled the dialog.';
        });
    };
});

function DialogController($scope, $mdDialog, $http) {
    $scope.data = '';
    $scope.hide = function() {
        $mdDialog.hide();
    };
    $scope.cancel = function() {
        $mdDialog.cancel();
    };
    $scope.answer = function() {
        $mdDialog.hide($scope.data);
    };
}