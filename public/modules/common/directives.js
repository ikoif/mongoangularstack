'use strict';

/**
 * Module definition for common components used by other all other app modules.
 */

angular.module('bookinfo.common.directives', ['bookinfo.common.utils'])

    .directive('convertType', ['convert', function (convert) {
        return {
            restrict: 'A',
            template: '{{convertType}}',
            scope: {
                type: '='
            },
            controller: function($scope, $element, $attrs){
                $scope.convertType = convert.type($scope.type);
            }
        };
    }])

    .directive('delayedInput', [function () {
        return {
            restrict: 'EA',
            scope: {
              callback: '&delayedInput'
            },
            controller: function ($scope, $element, $attrs){

              var delay = (function(){
                var timer = 0;
                return function(callback, ms){
                  clearTimeout (timer);
                  timer = setTimeout(callback, ms);
                };
              })();

              $(angular.element($element)).keyup(function() {
                  delay(function(){
                    // console.log('kvo')
                    $scope.callback();
                  }, 500 );
              });
            }
        };
    }])

    .directive('validatePrice', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function(value) {
                    var regex = /^[0-9]{1,4}\.[0-9]{2}$/;
                    ngModel.$setValidity('validate-price', regex.test(value));
                    return value;
                });
            }
        }
    })
    
    .directive('email', function() {
        
        var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return {
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$validators.email = function(modelValue, viewValue) {
                    if (ctrl.$isEmpty(modelValue)) {
                        // consider empty models to be valid
                        return true;
                    }

                    if (emailRegex.test(viewValue)) {
                        // it is valid
                        return true;
                    }

                    // it is invalid
                    return false;
                };
            }
        };
    });

    