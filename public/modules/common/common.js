'use strict';

/**
 * Module definition for common components used by other all other app modules.
 */

angular.module('bookinfo.common', [
    'bookinfo.common.utils',
    'bookinfo.common.directives'
])