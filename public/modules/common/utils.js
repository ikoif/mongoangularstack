'use strict';

/**
 * Module definition for common components used by other all other app modules.
 */

angular.module('bookinfo.common.utils', [])
    .factory('api', function ($rootScope, $http, $window) {
        var api = {name:'apito'}
        // var papi = new Papi(window.location.origin + '/pub/1.0');
        // api.papi = papi;
        return api;
    })

    .factory('notification', function ($rootScope, $http, $window) {
        return function(type, msg, timeout, clickCallback, template, overlay, layout){

            if(!timeout) timeout = 4000

            var n = noty({
                text        : msg,
                type        : type,
                dismissQueue: true,
                modal       : false,
                maxVisible  : 4,
                timeout     : timeout,
                layout      : 'topLeft',
                theme       : 'defaultTheme'
            });

        };
    })

    .factory('loadGenres', function(){
        return function(){
            return [
                '82-1 Поезия. Стихове. Стихотворения',
                '82-12 Поетическа драматургия',
                '82-13 Епична поезия',
                '82-14 Лирична поезия',
                '82-15 Дидактическа поезия. Дидактично-философски стихотворения',
                '82-16 Описателна поезия',
                '82-17 Сатирична и хумористична поезия',
                '82-191 Алегорическа поезия, Алегорични стихотворения. Басни. Фабли. Притчи. Параболи. Метаморфози.',
                '82-192 Мелодична поезия. Поезия с музика',
                '82-193 Малки поетични форми',
                '82-194 Поетически алманаси',
                '82-2 Драми. Пиеси',
                '82-21 Трагедии',
                '82-22 Комедии',
                '82-23 Мелодрами. Трилъри. Мистерии. Популярен театър',
                '82-24 Исторически пиеси',
                '82-27 Монолози. Рецитали. Пиеси за един актьор (за моноспектакли)',
                '82-291 Средновековна и ренесансова драма',
                '82-292 Народен театър. Фолклорни драми',
                '82-293 Музикална драма. Либрето',
                '82-293.1 Голяма опера. Опера серна',
                '82-293.2 Комична опера. Опера-буфа. Оперети',
                '82-293.4 Музикална драма. Драма, декламирана с музикален съпровод. Мелодрама',
                '82-3 Проза. Художествена проза',
                '82-31 Романи',
                '82-311.1 Психологически романи. Интроспективни романи, разкази в първо лице',
                '82-311.2 Романи на нрави и характери. описание на всекидневния живот. Реалистични романи, представящи отделни аспекти на живота',
                '82-311.3 Романи на действието. Приключенски романи',
                '82-311.4 Социални романи. Пикарескни романи. Симплицикади. Работнически романи. Морски романи',
                '82-311.5 Фриволни, комични романи.',
                '82-311.6 Исторически романи. Политически романи. Военни романи',
                '82-311.7 Тенденциозни, дидактични, пропагандистски романи',
                '82-311.8 Романи за пътешествия. Екзотични романи',
                '82-311.9 Научно-фантастични романи',
                '82-312.1 Философски романи',
                '82-312.2 Религиозни романи. Романи на мистични или морални теми',
                '82-312.3 Буколически романи. Романи за живота на село',
                '82-312.4 Криминални романи. Детективски романи. Романи за мистерии, напрежение. Трилъри.',
                '82-312.5 Популярна, евтина, сензационна литература',
                '82-312.6 Автобиографични романи. Художествени биографии',
                '82-312.7 Епистоларни романи. Диалогични романи.',
                '82-312.8 Символични, кабалистични романи',
                '82-312.9 Фентъзи, фантастични романи',
                '82-313.1 Сатирични романи',
                '82-313.2 Утопични романи',
                '82-32 Разкази. Повести. Новели',
                '82-34 Легенди. Измислени истории. Литературни приказки',
                '82-341 Весели истории',
                '82-342 Дидактични истории с теоретични и практически поуки',
                '82-343 Митове. легенди. Приказки',
                '82-344 Истории за фантастични, свръхестествени, ужасяващи събития',
                '82-36 Кратки истории. Анекдоти',
                '82-39 Старинни (напр. средновековни) романи',
                '82-4 Есета',
                '82-5 Речи',
                '82-6 Писма. Епистолярно творчество. Кореспонденция. Писма, които не са художествени произведения. Други произведения в епистолярна форма',
                '82-7 Сатирична проза. Хумор, епиграма, пародия и др.',
                '82-8 Сборници със смесено съдържание. Антологии. Христоматии',
                '82-82 Сборници със смесено съдържание. Откъси, извлечения. Сборници от избрани поетични произведения (антологии). христоматии. Флорилегии',
                '82-83 Философски или аналитични диалози. Разговори на различни теми',
                '82-84 Максими. Афоризми. Сентенции. Пословици. Поговорки. Крилати фрази',
                '82-9 Други форми и жанрове',
                '82-91 Популярна литература',
                '82-92 Периодична литература (от списания, вестници и др. с литературна тематика), Публицистика. Литература, посветена на определено събитие. Полемични, политически съчинения. Памфлети',
                '82-93 Литература за деца и юноши',
                '82-94 Историята като литературен жанр. Исторически публикации. Истография',
                '82-95 Критика като литературен жанр. Обзори. Литературни мистификации',
                '82-96 Научни и философски произведения като литература',
                '82-97 Религиозна литература в цялост',
                '82-98 Символи. Емблеми. Девизи. Лични максими. Ребуси. Пъзли. Анаграми',
                '82-991 Дисертации',
                '82-992 Пътеписи',
                '82-993 Фриволна литература. Двусмислени разкази. Непристойни истории. еротични произведения. Порнографска литература.',
                '82-994 Ексцентрична литература. Литература на абсурда. Визионерска литература',
                '82-995 Литературни фалшификати. Пастиши. Въображаеми, предполагаеми произведения.',
                '82.0 Теория, техника и методология на литератърата',
                '82.02/.09 Специални определители за теория, теника и методология на литературата',
                '82.02 Литературни школи, направления и движения.',
                '82.09 Литературна критика. Литературознание',
                '82.091 Сравнително литературознание. Сравнителна литература',
                '82’01/’06 Специални определители за периоди и етапи от развитието на литературата',
                '82’01 Стар или ранен период. Най-ранни форми',
                '82’04 Среден период. Средновековен период',
                '82’06 Съвременен период'
            ];
        }
    })

    .factory('utils', function ($rootScope, $http, $window, $const) {
        return {
            parseBoolean: function(val){
                return val === 'true' || val
            }
        }
    })

    .factory('convert', function ($rootScope, $http, $window, $const) {
        return {
            type: function(val){
                switch(val){
                    case 'publisher':
                        return 'Издател'
                        break;
                    case 'merchant':
                        return 'Книжарница'
                        break;
                    case 'bookstore':
                        return 'Верига книжарници'
                        break;
                    case 'onlinestore':
                        return 'Онлайн книжарница'
                        break;
                    case 'library1':
                        return 'Библиотека'
                        break;
                    case 'library2':
                        return 'Читалище'
                        break;
                    case 'institution':
                        return 'Институция'
                        break;
                    case null:
                        return 'Админ'
                        break;
                    default:
                        return 'Издател'
                        break;
                }
            },
            role: function(val){
                switch(val){
                    case 'pub':
                        return 'Издател'
                        break;
                    case 'user':
                        return 'Потребител'
                        break;
                    case 'admin':
                        return 'Админ'
                        break;
                    default:
                        return val
                        break;
                }
            },
            feedType: function(val){
                switch(val){
                    case 'updated':
                        return 'редактира'
                        break;
                    case 'created':
                        return 'добави'
                        break;
                    default:
                        return val
                        break;
                }
            },
            date: function(val, full){

                function getPrettyDate(date) {
                    if(!date) return date

                    var a = ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
                    var b = ['Януари', 'Февруари', 'Март', 'Април', 'Май', 'Юни', 'Юли', 'Август', 'Септември', 'Октомври', 'Ноември', 'Декември'];
                    var c = new Date(date);
                    var w = a[c.getDay()];
                    var d = c.getDate()<10 ? '0'+c.getDate() : c.getDate()
                    var e = b[c.getMonth()];
                    var y = c.getFullYear();
                    var time = '';

                    if(full){
                        var hoursZero = c.getHours() < 10 ? '0' : '';
                        var minutesZero = c.getMinutes() < 10 ? '0' : '';
                        var secondsZero = c.getSeconds() < 10 ? '0' : '';

                        time = ' в ' +hoursZero+c.getHours()+ ':' +minutesZero+c.getMinutes()+ ':' +secondsZero+c.getSeconds();
                    }

                    return d + ' ' + e + ' ' + y + time
                }

                return getPrettyDate(val)
            },
            array: function(uglyArray, concatination, andWord, comma) {
                if(!comma) comma = ','
                if(uglyArray !== undefined && typeof(uglyArray) === 'object'){
                    if(concatination === undefined) { concatination = false }

                    var prettyArray = '';

                    if(concatination){

                        if(andWord === undefined) { andWord = 'and' }

                        if(uglyArray.length > 1) {
                            uglyArray.splice((uglyArray.length - 1), 0, andWord);
                        }

                        prettyArray = uglyArray.join(comma + ' ');

                        if(uglyArray.length > 1) {
                            // without the first "," from the end
                            prettyArray = prettyArray.substr(0, prettyArray.lastIndexOf(comma)) + prettyArray.substr(prettyArray.lastIndexOf(comma)+1);
                            // doing the same to remove the second ","
                            prettyArray = prettyArray.substr(0, prettyArray.lastIndexOf(comma)) + prettyArray.substr(prettyArray.lastIndexOf(comma)+1);
                        }
                    }else{
                        prettyArray = uglyArray.join(comma + ' ');
                    }

                    return prettyArray
                }else{
                    return uglyArray
                }
            },
            price: function(val) {
                return parseFloat(val).toFixed(2) + ' лв'
            },
            substring: function(value, howMuch){
                var text = value;
                if(value.length > howMuch){
                    text = value.substr(0,howMuch) + ' ...'
                }
                return text
            }
        }
    })
    .factory('checkIfChanged', function ($rootScope, $http, $window, $const, convert) {
        var arr_diff = function (a1, a2)
        {
            var a = [],
                diff = [];
            for (var i = 0; i < a1.length; i++)
                a[a1[i]] = true;
            for (var i = 0; i < a2.length; i++)
                if (a[a2[i]]) delete a[a2[i]];
                else a[a2[i]] = true;
            for (var k in a)
                diff.push(k);
            return diff.length !== 0;
        }

        return function(element, old){

            element.changed = [];

            var current = element.data;

            if(current.price != old.price){
                element.changed.push({
                    text: "цена",
                    changedOld : convert.price(old.price),
                    changedNew : convert.price(current.price)
                })
            }
            if(current.inStock != old.inStock){
                element.changed.push({
                    text: "наличност",
                    changedOld : old.inStock ? 'налична' : 'неналична',
                    changedNew : current.inStock ? 'налична' : 'неналична'
                })
            }
            if(current.publishedOn != old.publishedOn){
                element.changed.push({
                    text: "дата на пулбикуване",
                    changedOld : convert.date(old.publishedOn),
                    changedNew : convert.date(current.publishedOn)
                })
            }
            if(current.republishedOn != old.republishedOn){
                element.changed.push({
                    text: "дата на допечатка",
                    changedOld : convert.date(old.republishedOn),
                    changedNew : convert.date(current.republishedOn)
                })
            }

            // console.log(element.changed)
        }
    })
    .factory('dataDiff', function ($rootScope, $http, $window, $const, convert) {
        var arr_diff = function (a1, a2)
        {
            var a = [],
                diff = [];
            for (var i = 0; i < a1.length; i++)
                a[a1[i]] = true;
            for (var i = 0; i < a2.length; i++)
                if (a[a2[i]]) delete a[a2[i]];
                else a[a2[i]] = true;
            for (var k in a)
                diff.push(k);
            return diff.length !== 0;
        }

        return function(element, diffObj){

            element.changed = [];

            var current = element.data;

            if(typeof diffObj.inStock !== 'undefined'){
                element.changed.push({
                    text: "наличност",
                    changedNew : diffObj.inStock ? 'налична' : 'неналична'
                })
            }

            if(typeof diffObj.price !== 'undefined'){
                element.changed.push({
                    text: "цена",
                    changedNew : convert.price(diffObj.price)
                })
            }

            if(typeof diffObj.publishedOn !== 'undefined'){
                element.changed.push({
                    text: "дата на пулбикуване",
                    changedNew : convert.date(diffObj.publishedOn)
                })
            }

            if(typeof diffObj.republishedOn !== 'undefined'){
                element.changed.push({
                    text: "дата на допечатка",
                    changedNew : convert.date(diffObj.republishedOn)
                })
            }
            
            if(typeof diffObj.title !== 'undefined'){
                element.changed.push({
                    text: "заглавие",
                    changedNew : diffObj.title
                })
            }
            // console.log(element.changed)
        }
    })

    .service('Content.user',
    ['$rootScope', 'api', '$q', 'notification', '$const', 'utils',
    function ($rootScope, api, $q, notification, $const, utils) {

        var self = this;

        self.loadSelf = function (){
            return api.currentUser()
        };

        self.loadUser = function (userId){
            return api.getUser(userId)
        }

        self.getUser = function (userId){

            var nameLen = 30;


            var deferred = $q.defer();
            if(userId){
                self.loadUser(userId)
                    .then(function(user){
                        if(typeof user.companyName !== 'undefined'){
                            if(user.publisherLogo === null) user.publisherLogo = $const.IMAGE.user.ph
                            user.companyVAT = utils.parseBoolean(user.companyVAT);

                            if(user.companyName !== null){
                                if(user.companyName.length > nameLen){    
                                    user.companyNameDisplay = user.companyName.substr(0,nameLen) + '...'
                                } else {
                                    user.companyNameDisplay = user.companyName;
                                }
                            }

                            deferred.resolve(user);
                        }else{
                            deferred.reject('User not found');
                        }
                    }, function (err){
                        deferred.reject(err);
                    });

            }else{
                self.loadSelf()
                    .then(function(user){
                        // pass the user object to the $rootScope so it can be used accross all templates
                        if(user.publisherLogo === null) user.publisherLogo = $const.IMAGE.user.ph
                        
                        user.companyVAT = utils.parseBoolean(user.companyVAT);

                        if(user.brandName !== null){
                            if(user.brandName.length > nameLen){    
                                user.companyNameDisplay = user.brandName.substr(0,nameLen) + '...'
                            } else {
                                user.companyNameDisplay = user.brandName;
                            }
                        }

                        $rootScope.user = user;
                        
                        deferred.resolve(user);
                    }, function (err){
                        console.log(err)
                        notification('alert', 'There was a problem logging you in, please log in again. This is an issue with us, not with your account', 3000)
                        deferred.reject(err);
                    });
            }

            return deferred.promise;
        };

    }]);