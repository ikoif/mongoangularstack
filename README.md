# MongoAngularStack

A barebone Single Page NodeJS Web Application which utilises MongoDB as a data base, ExpressJS as a server-side web framework and AngularJS as a client-side framework. It has minimal routes setup, and serving a main layout file. It has client-side routing and a modular structure that allows flexibility, reusability and easy dependency managment. It uses Jade lang as a templating engine which allows modular structure and code simplicity in the HTML output. SCSS is used for SASS compliation which enhances the CSS code readability and easy use of advanced syntax. 

## Installation
1. Download the repository
2. Install npm modules: `npm install`
3. Install bower dependencies `bower install`
4. Build html and css `gulp build`
4. Start up the server: `npm start`
(-) Run the JADE and SCSS watchers for realtime build: `gulp watch`
5. View in browser at http://localhost:3007 (or whatever port)

## Feautures:
- Automatic modules lookup and build
- Easy adding of extra pages, routing and modules
- Concurrent watchers
- AngularJS, Mongo and Material UI ready to go