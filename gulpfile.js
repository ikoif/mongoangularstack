var path = require('path');
var fs = require('fs');
var gulp = require('gulp');
var jade = require('gulp-jade');
var sass = require('gulp-sass');
var chalk = require('chalk');

var paths = {
    css: 'public/styles/',
    cssOutput: [this.css + 'main.scss', this.css + 'bootstrap_custom.scss' ],
    base : './public/modules/',
    partials : './public/views/partials/',

    destJade: './public/modules/',
    destScss: './public/',
    exclude: ''
}

var maskJade = '**/*.jade';
var maskScss = '*.scss';

function getDirectories(srcpath) {
  return fs.readdirSync(srcpath).filter(function(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  });
}

gulp.task('build-modules', function(){

    getDirectories(paths.base).forEach(function(e){
        
        gulp.src([paths.base + e + '/'  + maskJade])
            .pipe(jade())
            .pipe(gulp.dest(paths.base + e))
            .on('end', function(){console.log('completed build in ' + paths.base + e + '/' + maskJade)})
            .on('error', function(e){console.log('e')})
    })

    getDirectories(paths.partials).forEach(function(e){
        
        gulp.src([paths.partials + e + '/'  + maskJade])
            .pipe(jade())
            .pipe(gulp.dest(paths.partials + e))
            .on('end', function(){console.log('completed build in ' + paths.partials + e + '/' + maskJade)})
            .on('error', function(e){console.log('e')})
    })
        
});

gulp.task('sass', function () {
  gulp.src([paths.css + 'bootstrap_custom.scss' ])
    .pipe(sass({includePaths: './public/bower_components/'})
        .on('error', sass.logError))
    .pipe(gulp.dest(paths.destScss));
});

gulp.task('build', ['build-modules', 'sass']);


//*****************
//*
//*  Watchers
//*
//*****************


gulp.task('watch', function() {

    var input = [paths.base].map(function(i) { return i + maskJade })
    var input2 = [paths.partials].map(function(i) { return i + maskJade })
    input = input.concat(input2)
    gulp.watch(input).on('change', function(event) {
        var pathArr = event.path.split('\\');
        var file = pathArr[pathArr.length-1];

        // 5 for '.jade'
        var folderName = file.substr(0, file.length - 5);

        console.log('File ' + chalk.magenta.bold(event.path) + ' was ' + event.type + ', creating '+ folderName +'.html in /' + folderName);
        
        var src =  paths.base + folderName
        gulp.src(event.path, {base: src})
            .pipe(jade())
            .pipe(gulp.dest(src))
        .on('end', function(){console.log('... completed')})
    });

    // watcher for Scss changes
    gulp.watch(paths.css + maskScss, ['sass']);
});


gulp.task('default', ['build']);