'use strict';
var path = require('path');

// Production specific configuration
// =================================
module.exports = {
	addr: 'http://lexicon-bookscraper.cloudapp.net',
	db: {
		"database": "bookscraper_prod",
		"url" : "mongodb://localhost:27017/bookscraper_prod"
	}
};
