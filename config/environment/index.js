'use strict';

var path = require('path');
var _    = require('lodash');

var env;

if (process.env.NODE_ENV == null) {
    env = 'development';
    process.env.NODE_ENV = env;
}
process.env.NODE_ENV = env;

function requiredProcessEnv(name) {
    if(!process.env[name]) {
        throw new Error('You must set the ' + name + ' environment variable');
    }
    return process.env[name];
}

const projectRoot = path.normalize(__dirname + '/../../..');

// All configurations will extend these options
// ============================================
var all = {
    env: process.env.NODE_ENV,

    // Root path of server
    root: projectRoot,

    // Server port
    port: process.env.PORT || 3000,
	addr: 'localhost',

    // Secret for session, you will want to change this and make it an environment variable
    secrets: {
        session: 'c1NuR3AD71s?',
        authHashKey: 'abv85516de666a8d43aa9afc7d36ee1h9'
    },
    dataFolder: path.join(projectRoot, 'var'),
    db: {
        "database": "bookscraper_dev",
        "url" : "mongodb://localhost:27017/bookscraper_dev"
    }
};

// Export the config object based on the NODE_ENV
// ==============================================
console.log('RUNNING IN ENV:', process.env.NODE_ENV);
module.exports = _.merge(
    all,
    require('./' + requiredProcessEnv('NODE_ENV') + '.js') || {});